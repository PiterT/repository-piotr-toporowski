package fifteen_puzzle;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public class GameBoard {

    private final List<GameNode> startFields =
            Arrays.asList(new GameNode(1), new GameNode(2),
                    new GameNode(3), new GameNode(4), new GameNode(5),
                    new GameNode(6), new GameNode(7), new GameNode(8),
                    new GameNode(9), new GameNode(10), new GameNode(11),
                    new GameNode(12), new GameNode(13), new GameNode(14),
                    new GameNode(15));

    private final GameNode[][] fields = new GameNode[4][4];


    private void shuffle() {

    }

    public boolean isSuccess() {
        boolean isWin = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == 3 && j == 3) {
                    isWin = isWin && fields[i][j].getValue() == 0;
                } else {
                    isWin = isWin && fields[i][j].getValue() == (i * 4 + j) + 1;
                }
            }
        }
        return isWin;
    }

    public void move(FieldCoordinates userChoice) {
        if ( fields[userChoice.getRow()][userChoice.getCol()].isEmptyNode() ){
            throw new IllegalArgumentException("Is empty node");
        } else {

            Optional<GameNode> left = left(userChoice);
            Optional<GameNode> right = right(userChoice);
            Optional<GameNode> upper = upper(userChoice);
            Optional<GameNode> down = down(userChoice);

            boolean swapWithLeft = left.map(GameNode::isEmptyNode).orElse(false);
            boolean swapWithRight = right.map(GameNode::isEmptyNode).orElse(false);
            boolean swapWithUpper = upper.map(GameNode::isEmptyNode).orElse(false);
            boolean swapWithLowwer = down.map(GameNode::isEmptyNode).orElse(false);

            if( swapWithLeft){
                swap( userChoice.left(), userChoice );
            } else if( swapWithRight) {
                swap( userChoice.right(), userChoice);
            } else if ( swapWithUpper) {
                swap(userChoice.upper(), userChoice);
            } else if ( swapWithLowwer ) {
                swap (userChoice.lower(), userChoice);
            } else {
                System.out.println("This node can not be moved");
            }
        }


    }

    private Optional<GameNode> left(FieldCoordinates userChoice) {
        if( userChoice.getCol() < 1 ){
            return Optional.empty();
        } else {
            return Optional.of( fields[userChoice.getRow()][userChoice.getCol() -1]);
        }
    }

    private Optional<GameNode> right( FieldCoordinates userChoice) {
        if( userChoice.getCol() > 2 ) {
            return Optional.empty();
        } else {
            return Optional.of( fields[userChoice.getRow()][userChoice.getCol() +1] );
        }
    }

    private Optional<GameNode> upper(FieldCoordinates userChoice) {
        if( userChoice.getRow() < 1 ){
            return Optional.empty();
        } else {
            return Optional.of( fields[userChoice.getRow()-1][userChoice.getCol()]);
        }
    }

    private Optional<GameNode> down(FieldCoordinates userChoice) {
        if( userChoice.getRow() > 2 ){
            return Optional.empty();
        } else {
            return Optional.of( fields[userChoice.getRow()+1][userChoice.getCol()]);
        }
    }

    private void swap(FieldCoordinates nodeToSwap, FieldCoordinates userChoice) {
        GameNode temporaryCoordiantes = fields[nodeToSwap.getRow()][nodeToSwap.getCol()];
        fields[nodeToSwap.getRow()][nodeToSwap.getCol()]
                = fields[userChoice.getRow()][userChoice.getCol()];
        fields[userChoice.getRow()][userChoice.getCol()] = temporaryCoordiantes;
    }
}
