package fifteen_puzzle;

public class FieldCoordinates {

    private final int row;
    private final int col;

    FieldCoordinates(String userChoice) {

        String stringRow = userChoice.substring(0, 1).toUpperCase();
        String stringCol = userChoice.substring(1);

        this.col = Integer.valueOf(stringCol) - 1;

        switch (stringRow) {
            case "A":
                this.row = 0;
                break;
            case "B":
                this.row = 1;
                break;
            case "C":
                this.row = 2;
                break;
            case "D":
                this.row = 3;
                break;
            default:
                throw new IllegalArgumentException("Wrong coordinates");
        }
    }

    private FieldCoordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public FieldCoordinates left() {
        return new FieldCoordinates(row, col - 1);
    }

    public FieldCoordinates right() {
        return new FieldCoordinates(row, col + 1);
    }

    public FieldCoordinates upper() {
        return new FieldCoordinates(row + 1, col);
    }

    public FieldCoordinates lower() {
        return new FieldCoordinates(row - 1, col);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }
}
