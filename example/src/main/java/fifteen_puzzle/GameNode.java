package fifteen_puzzle;

public class GameNode {

    private int value;

    public GameNode(int value) {
        if (value >= 0 || value < 16) {
            this.value = value;
        } else {
            throw new IllegalArgumentException("Game node must in range 0 - 15");
        }
    }

    public int getValue() {
        return value;
    }

    public boolean isEmptyNode(){
        return value == 0;
    }
}
