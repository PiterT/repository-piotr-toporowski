package fifteen_puzzle;

import java.util.Scanner;

public class Game16 {


    public static void main(String[] args) {
        GameBoard gameBoard = new GameBoard();
        Scanner input = new Scanner(System.in);

        System.out.println("Start game: ");

        while (true) {

            gameBoard.toString();
            System.out.println("Enter coordinates");
            String nextMove = input.nextLine();

            gameBoard.move(new FieldCoordinates(nextMove) );

            if (gameBoard.isSuccess()) {
                System.out.println("Win game");
                break;
            } else {
                System.out.println("Next move: ");
            }
        }
    }
}
