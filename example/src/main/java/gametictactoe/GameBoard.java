package gametictactoe;

public class GameBoard {

    private String[][] fields = new String[3][3];
    private final String CROSS = "X";
    private final String CIRCLE = "O";
    private String lastSymbol = CROSS;
    private int counterMove = 0;
    private String empty = " " +
            "";

    public void dispalyFields() {
        System.out.printf("%3s %s %s \n", 1, 2, 3);
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    System.out.print("A|");
                    break;
                case 1:
                    System.out.print("B|");
                    break;
                case 2:
                    System.out.print("C|");
            }
            for (int j = 0; j < 3; j++) {
                System.out.print(fields[i][j] == null ? empty : fields[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public void move(FieldCoordinates fieldCoordinates) {
        if (empty.equals(fields[fieldCoordinates.getRow()][fieldCoordinates.getCol()])
                || CROSS.equals(fields[fieldCoordinates.getRow()][fieldCoordinates.getCol()])
                || CIRCLE.equals(fields[fieldCoordinates.getRow()][fieldCoordinates.getCol()])
                ) {
            System.out.println("Is not empty coordinates");
        } else {
            fields[fieldCoordinates.getRow()][fieldCoordinates.getCol()] = (lastSymbol.equals(CROSS) ? CIRCLE : CROSS);
            lastSymbol = lastSymbol.equals(CROSS) ? CIRCLE : CROSS;
            counterMove++;
        }
    }

    public int getCounterMove() {
        return counterMove++;
    }

    public boolean isSuccess() {
        boolean success = false;
        if (crossSuccess()
                || rowSuccess()
                || colSuccess()) {
            success = true;
        }
        return success;
    }

    private boolean rowSuccess() {
        return (lastSymbol.equals(fields[0][0])
                && lastSymbol.equals(fields[0][1])
                && lastSymbol.equals(fields[0][2]))
                ||
                lastSymbol.equals(fields[1][0])
                        && lastSymbol.equals(fields[1][1])
                        && lastSymbol.equals(fields[1][2])
                ||
                lastSymbol.equals(fields[2][0])
                        && lastSymbol.equals(fields[2][1])
                        && lastSymbol.equals(fields[2][2]);
    }

    private boolean colSuccess() {
        return
                (lastSymbol.equals(fields[0][0])
                        && lastSymbol.equals(fields[1][0])
                        && lastSymbol.equals(fields[2][0])
                        ||
                        lastSymbol.equals(fields[0][1])
                                && lastSymbol.equals(fields[1][1])
                                && lastSymbol.equals(fields[2][1])
                        ||
                        lastSymbol.equals(fields[0][2])
                                && lastSymbol.equals(fields[1][2])
                                && lastSymbol.equals(fields[2][2]));
    }

    private boolean crossSuccess() {
        return
                (lastSymbol.equals(fields[0][0])
                        && lastSymbol.equals(fields[1][1])
                        && lastSymbol.equals(fields[2][2])
                        ||
                        lastSymbol.equals(fields[2][0])
                                && lastSymbol.equals(fields[1][1])
                                && lastSymbol.equals(fields[0][2])
                );
    }

}
