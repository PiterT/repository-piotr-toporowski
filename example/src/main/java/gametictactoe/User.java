package gametictactoe;

public class User {

    private String nameUser;
    private long time;

    public User(String nameUser, long time) {
        this.nameUser = nameUser;
        this.time = time;
    }

    public String getNameUser() {
        return nameUser;
    }

    public long getTime() {
        return time;
    }
}
