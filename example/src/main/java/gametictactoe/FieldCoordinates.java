package gametictactoe;

public class FieldCoordinates {

    private int row;
    private int col;

    //coordiantes A1 A=row   1=col
    FieldCoordinates(String coordinates) {
        String rowAsString = coordinates.substring(0, 1).toUpperCase();
        String colAsString = coordinates.substring(1).toUpperCase();

        switch (rowAsString) {
            case "A":
                this.row = 0;
                break;
            case "B":
                this.row = 1;
                break;
            case "C":
                this.row = 2;
        }
        this.col = Integer.valueOf(colAsString) - 1;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }
}
