package gametictactoe;

import java.util.*;
import java.io.*;
import java.math.*;
import java.util.stream.Collectors;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public int solution(int[] array) {
        // write your code in Java SE 8
        List<Integer> list = new ArrayList<>();
        int num = 1;
        for( int i = 0; i < array.length; i++){
            list.add(array[i]);
        }

        while ( list.contains(num) ){
            num++;
        }
        return num;


    }
}