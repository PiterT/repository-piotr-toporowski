package data.structures.stack;

public class Stack<T> {

    private StackNode<T> head = null;
    private int stackSize = 0;

    public void add(T data) {
        this.head = new StackNode<>(data, head);
        this.stackSize++;
    }

    public T peek() {
        if (stackSize == 0) {
            return null;
        } else {
            return head.getData();
        }
    }

    public T pop() {
        if (stackSize == 0) {
            return null;
        } else {
            stackSize--;
            T data = head.getData();
            head = head.getNext();
            return data;
        }
    }

}
