package data.structures.stack;

public class StackNode<T> {

    private final T data;
    private final StackNode<T> next;

    StackNode(T data, StackNode<T> next) {
        this.data = data;
        this.next = next;
    }

    public StackNode<T> getNext() {
        return next;
    }

    public T getData() {
        return data;
    }

}
