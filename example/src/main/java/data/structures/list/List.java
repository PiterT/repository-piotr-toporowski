package data.structures.list;

public class List<T> {

    private ListNode<T> head = null;
    private ListNode<T> tail = null;
    private int listSize = 0;

    public void add(T data) {
        if (listSize == 0) {
            ListNode<T> newNode = new ListNode<>(head, tail, data);
            head = newNode;
            tail = newNode;
        } else {
            ListNode<T> newNode = new ListNode<>(tail, null, data);
            tail.setNext(newNode);
            tail = newNode;
        }
        listSize++;
    }

    public boolean contains(T data) {
        if (data == null) {
            return false;
        } else {
            ListNode current = head;
            for (int i = 0; i < listSize; i++)
                if (current.getData().equals(data)) {
                    return true;
                } else {
                    current = current.getNext();
                }
        }
        return false;
    }

    @Override
    public String toString() {
        return "List{" +
                "head=" + head +
                ", tail=" + tail +
                ", listSize=" + listSize +
                '}';
    }
}
