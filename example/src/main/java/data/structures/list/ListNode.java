package data.structures.list;

public class ListNode<T> {

    private ListNode<T> next;
    private ListNode<T> previous;
    private final T data;

    ListNode(ListNode<T> next, ListNode<T> previous, T data) {
        this.next = next;
        this.previous = previous;
        this.data = data;
    }

    public ListNode<T> getNext() {
        return next;
    }

    public ListNode<T> getPrevious() {
        return previous;
    }

    public T getData() {
        return data;
    }

    public void setNext( ListNode<T> next) {
        this.next = next;
    }

    public void setPrevious(ListNode<T> previous) {
        this.previous = previous;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "next=" + next +
                ", previous=" + previous +
                ", data=" + data +
                '}';
    }
}
