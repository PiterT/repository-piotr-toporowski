package turtlegraphics;

public class GameBoard {

    private String[][] field = new String[20][20];
    private String symbol = " *";
    FieldCoordinates fieldCoordinates = new FieldCoordinates();

    public void display() {
        System.out.println("GameBoard");
        System.out.println();
        System.out.print("    01 ");
        for (int i = 1; i < 20; i++) {
            System.out.printf("%02d ", i + 1);
        }
        System.out.println();

        for (int i = 0; i < 20; i++) {
            System.out.printf("%02d  ", i + 1);
            for (int j = 0; j < 20; j++) {
                System.out.print(field[i][j] == null ? "   " : (field[i][j] + " "));
            }
            System.out.println();
        }
    }

    public void move(String userChoice) {

        fieldCoordinates.setPosition(userChoice);
        //if (fieldCoordinates.isPenDown()) {
            field[fieldCoordinates.getRow()][fieldCoordinates.getCol()] = symbol;

    }
}
