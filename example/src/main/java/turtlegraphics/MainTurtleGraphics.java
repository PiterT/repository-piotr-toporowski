package turtlegraphics;

import java.util.Scanner;

public class MainTurtleGraphics {

    public static void main(String[] args) {

        System.out.printf("%s %6s\n", "1", "Go up");
        System.out.printf("%s %6s\n", "2", "Go down");
        System.out.printf("%s %6s\n", "3", "Go right");
        System.out.printf("%s %6s\n", "4", "Go left");
        System.out.printf("%s %6s\n", "5", "Pen down or pen up");
        System.out.printf("%s %6s\n", "6", "Display the 20-by-20 array");
        System.out.printf("%s %6s\n", "E", "End program");
        GameBoard gameBoard = new GameBoard();
        Scanner input = new Scanner(System.in);
        System.out.println("Start!");
        System.out.println("Enter move");
        String userChoice = input.nextLine();
        FieldCoordinates fieldCoordinates = new FieldCoordinates();
//        FieldCoordinates fieldCoordinates;

        while (!userChoice.equalsIgnoreCase("E")){
            System.out.println();
            System.out.println("Start!");
            System.out.println(fieldCoordinates.isPenDown());
            gameBoard.display();
            System.out.println("Enter a move: ");
            userChoice = input.nextLine();
            gameBoard.move(userChoice);
        }

    }
}
