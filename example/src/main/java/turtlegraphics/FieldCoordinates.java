package turtlegraphics;

public class FieldCoordinates {
    @Override
    public String toString() {
        return "FieldCoordinates{" +
                "row=" + row +
                ", col=" + col +
                ", penDown=" + penDown +
                '}';
    }
    private int row;
    private int col;
    private boolean penDown = true;

    public FieldCoordinates() {
    }

    public void setPosition(String coordinates) {
        switch (coordinates) {
            case "1":
                this.col++;
                break;
            case "2":
                this.col -= 1;
                break;
            case "3":
                this.row += 1;
                break;
            case "4":
                this.row -= 1;
                break;
//            case "5":
//                this.penDown = true ? false : false;
        }
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isPenDown() {
        return penDown;
    }

}   //end class
